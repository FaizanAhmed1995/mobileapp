import React from 'react';
import { Text } from 'react-native';
import {FontAwesomeIcons} from "./TabBarIcon";

export default class Header extends React.Component {
    render() {
        const {navigation} = this.props;
        return <Text onPress={() =>
            navigation.openDrawer()}><FontAwesomeIcons name="bars"/></Text>;
    }
}
