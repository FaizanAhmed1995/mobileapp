import React from 'react';
import { Icon } from 'expo';

import Colors from '../constants/Colors';

export class TabBarIcon extends React.Component {
  render() {
    return (
      <Icon.Ionicons
        name={this.props.name}
        size={26}
        style={{ marginBottom: -3 }}
        color={this.props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
      />
    );
  }
}
export class FontAwesomeIcons extends React.Component {
    render() {
        const {customStyles, size} = this.props;
        return (
            <Icon.FontAwesome
                name={this.props.name}
                size={size || 23}
                style={[{marginBottom: -3}, customStyles]}
                color={this.props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
            />
        );
    }
}