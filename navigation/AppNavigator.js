import React from 'react';
import { createSwitchNavigator, createStackNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import SignupScreen from '../screens/SignupScreen';
import VerifyForgotPasswordScreen from '../screens/VerifyForgotPasswordScreen';
import ForgotScreen from '../screens/ForgotScreen';


const AuthStack = createStackNavigator({
    Signup: {
        // `ProfileScreen` is a React component that will be the main content of the screen.
        screen: SignupScreen,
        // When `ProfileScreen` is loaded by the StackNavigator, it will be given a `navigation` prop.

        // Optional: When deep linking or using react-navigation in a web app, this path is used:
        path: 'signup',
        // The action and route params are extracted from the path.

        // Optional: Override the `navigationOptions` for the screen
        navigationOptions: ({navigation}) => ({
            tabBarLabel: 'SignUp',
            headerLeft: null,
        }),
    },
    Forgot: {
        // `ProfileScreen` is a React component that will be the main content of the screen.
        screen: ForgotScreen,
        // When `ProfileScreen` is loaded by the StackNavigator, it will be given a `navigation` prop.

        // Optional: When deep linking or using react-navigation in a web app, this path is used:
        path: 'forgot',
        // The action and route params are extracted from the path.

        // Optional: Override the `navigationOptions` for the screen
        navigationOptions: ({navigation}) => ({
            tabBarLabel: 'Forgot Password',
            headerLeft: null,
        }),
    },
    VerifyForgotPassword: {
        // `ProfileScreen` is a React component that will be the main content of the screen.
        screen: VerifyForgotPasswordScreen,
        // When `ProfileScreen` is loaded by the StackNavigator, it will be given a `navigation` prop.

        // Optional: When deep linking or using react-navigation in a web app, this path is used:
        path: 'verifyForgot',
        // The action and route params are extracted from the path.

        // Optional: Override the `navigationOptions` for the screen
        navigationOptions: ({navigation}) => ({
            tabBarLabel: 'Verify Forgot Password',
            headerLeft: null,
        }),
    },
}, {
    initialRouteName: 'Signup'
});

export default createSwitchNavigator({
  // You could add another route here for authentication.
  // Read more at https://reactnavigation.org/docs/en/auth-flow.html
  Auth: AuthStack,
  Main: MainTabNavigator,
});