import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createDrawerNavigator, createBottomTabNavigator } from 'react-navigation';

import MembershipStatusScreen from '../screens/MembershipStatusScreen';
import HomeScreen from '../screens/HomeScreen';
import ProfileScreen from '../screens/ProfileScreen';
import SettingsScreen from '../screens/SettingScreen';
import SignupScreen from '../screens/SignupScreen';
import DashboardScreen from '../screens/DashboardScreen';
import MembershipScreen from '../screens/MembershipScreen';
import VerifyMembershipScreen from '../screens/VerifyMembership';
import ForgotScreen from '../screens/ForgotScreen';
import VerifyForgotPasswordScreen from '../screens/VerifyForgotPasswordScreen';
const SignupStack = createStackNavigator({
      Signup: SignupScreen
})
const DashboardStack = createStackNavigator({
    Dashboard : DashboardScreen
})

const SigninStack = createStackNavigator({
  Signin: HomeScreen,
});
const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
});
const ProfileStack = createStackNavigator({
  Profile: ProfileScreen,
});

const MembershipStatusStack= createStackNavigator({
  Status: MembershipStatusScreen,
})

const MembershipStack= createStackNavigator({
  Membership: MembershipScreen,
})

const VerifyMembershipStack = createStackNavigator({
   VerifyMembership: VerifyMembershipScreen,
})

// HomeStack.navigationOptions = {
//   tabBarLabel: 'Home',
//   tabBarIcon: ({ focused }) => (
//     <TabBarIcon
//       focused={focused}
//       name={
//         Platform.OS === 'ios'
//           ? `ios-information-circle${focused ? '' : '-outline'}`
//           : 'md-information-circle'
//       }
//     />
//   ),
// };
export default  createDrawerNavigator({
    SignupStack,
    DashboardStack,
    // HomeStack,
    SettingsStack,
    ProfileStack,
    MembershipStatusStack,
    MembershipStack,
    VerifyMembershipStack,
}, {
    headerMode: 'float',
        navigationOptions: ({navigation}) => ({
        headerStyle: {backgroundColor: '#4C3E54'},
        title: 'Welcome!',
        headerTintColor: 'white',
    })
})


// SettingsStack.navigationOptions = {
//   tabBarLabel: 'Settings',
//   tabBarIcon: ({ focused }) => (
//     <TabBarIcon
//       focused={focused}
//       name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}
//     />
//   ),
// };

// createStackNavigator({
//     DrawerStack: { screen: DrawerStack }
// }, {
//     headerMode: 'float',
//     navigationOptions: ({navigation}) => ({
//         headerStyle: {backgroundColor: '#4C3E54'},
//         title: 'Welcome!',
//         headerTintColor: 'white',
//     })
// })
