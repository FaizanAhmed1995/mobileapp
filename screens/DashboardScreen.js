import React from 'react';
import {View,Text,StyleSheet,TextInput,TouchableOpacity} from 'react-native';
import {Header} from "../components"
//import Menu,{MenuItem,MenuDivider} from 'react-native-material-menu';

export default class Dashboard extends React.Component{

  static navigationOptions = ({navigation}) => ({
      headerLeft: <Header navigation={navigation}/>
  });


  render(){
      return(
          <View style={styles.pagecolor}>
       
            <View style={styles.card}>
                 <Text style={styles.HeadingText}> Points Earned </Text>
                 <Text style={styles.NormalText}> 50 </Text> 
                </View>
                <View style={styles.card}>
                 <Text style={styles.HeadingText}> Points Spend </Text>
                 <Text style={styles.NormalText}> 20 </Text>
                </View>
                <View style={styles.card}>
                 <Text style={styles.HeadingText}>  User Count </Text>
                 <Text style={styles.NormalText}> 6 </Text>
                </View>
                <View style={styles.card}>
                 <Text style={styles.HeadingText}> Direct Referrals </Text>
                 <Text style={styles.NormalText}> 3 </Text>
                </View>
                <View style={styles.card}>
                 <Text style={styles.HeadingText}> Indirect Referrals </Text>
                 <Text style={styles.NormalText}> 4 </Text>
                </View>
                
              </View>
      )
  }


}

const styles= StyleSheet.create({
    pagecolor: {
        backgroundColor: '#fff',
        flex:1,
        flexDirection:'column',
       
      },
      card:{
        backgroundColor: '#eee',
       borderWidth:1,
    borderRadius: 5,
   alignSelf:'center',
      width: 200,
      height: 100,
     marginTop:15,
     paddingTop:10,
     paddingLeft:10,
      },
      HeadingText:{
          fontSize:20,
          fontWeight:'200',  
              },
              NormalText:{
                fontSize:20,
                fontWeight:'100',
                paddingLeft:10,  
                    },
})