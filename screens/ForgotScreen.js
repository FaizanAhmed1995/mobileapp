import React from 'react';
import {View,Text,StyleSheet,TextInput,TouchableOpacity} from 'react-native';


export default class Forgot extends React.Component{
    static navigationOptions = {
        header: null
      };
render()
{
return(
   <View style={styles.pagecolor}> 
    <View style={{marginTop:70}} >  
        <Text style={styles.CreateAccountText}> Recover Password  </Text>
    </View>
        <Text style={styles.signupText}> Email </Text>
        <View style={styles.signupBox}>
              <TextInput
              style={styles.signupPlaceholderText} 
              placeholder='Enter Email'   
              />
        </View>
         <Text style={{fontSize:20,paddingLeft:10}}> A verification link will be send to your email.</Text>
        <View style={styles.loginView}>
        <TouchableOpacity style={styles.login}
        onPress={()=>{this.props.navigation.navigate('Signin')}}> 
        
              <Text style={styles.loginText}> Proceed </Text>
             </TouchableOpacity>
        </View>
   </View>

   )
}

}


const styles=StyleSheet.create({
    pagecolor: {
        backgroundColor: '#fff',
        flex:1
      },
      CreateAccountText:{
          paddingTop:60,
          flexDirection:'row',
          justifyContent:'center',
          fontSize:50,
          fontWeight:'bold',
          color:'#EF5552',
          paddingLeft:15,
        },
        signupBox:{
            height: 50,
            width: 400,
            borderColor: 'gray', 
            borderWidth: 1,
            alignSelf:'center',
            borderRadius:10,
            backgroundColor:'white'
           
        },
        signupText:{
            fontSize:25,
            fontWeight:'100',
            flexDirection:'row',
            justifyContent:'flex-start',
            paddingLeft:10,
            paddingTop:5,
            paddingBottom:5,
            color:'#EF5552',
        },
        signupPlaceholderText:{
            fontSize:20,
            fontWeight:'100',
            flexDirection:'row',
            justifyContent:'center',
            paddingLeft:20,
            paddingTop:5,
        },
        login:{
            height: 60,
            width: 280,
            alignSelf:'center',
            borderRadius:10,
            backgroundColor:'#EF5552',
        },
        loginText:{
            alignSelf:'center',
            color:'white',
            fontSize:25,
            fontWeight:'bold',
            paddingTop:7
        },
        loginView:{
            paddingTop:10
        }
})