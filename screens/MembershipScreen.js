import React from 'react';
import {ScrollView,ListView,TouchableOpacity,StyleSheet,View,Text} from 'react-native';
import {Header} from "../components"



export default class Membership extends React.Component{
    static navigationOptions = ({navigation}) => ({
        headerLeft: <Header navigation={navigation}/>
    });

render(){

    return(
       <View style={styles.pagecolor}>
          <View style={styles.cardPosition}>
          <Text style={{fontSize:20, fontWeight:'400' }}> Get Membership  </Text>
         <View style={styles.card}>
               <View style={{flexDirection:'row'}}>
                   <View>
                <Text style={{fontSize:18}}> Company </Text>
                <Text style={{fontSize:18}}> CoinCap </Text>
                <Text style={{fontSize:18}}> CoinMarket </Text>
                    </View>
                    
                    <View style={{paddingLeft:100}} >
                    <Text style={{fontSize:18}}> Membership Types </Text>
                    
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                    <TouchableOpacity>
                        <Text style={{fontSize:18,color:'green'}}> Bronze </Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={{fontSize:18,color:'green'}}> Silver </Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={{fontSize:18,color:'green'}}> Gold </Text>
                    </TouchableOpacity>
                     </View>
                     <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                    <TouchableOpacity>
                        <Text style={{fontSize:18,color:'green'}}> Bronze </Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={{fontSize:18,color:'green'}}> Silver </Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={{fontSize:18,color:'green'}}> Gold </Text>
                    </TouchableOpacity>
                     </View>

                    </View>

             </View>

                    </View>
              </View>
          </View>
    )
}


}

const styles=StyleSheet.create({
    pagecolor: {
        backgroundColor: '#fff',
        flex:1,
        flexDirection:'column',
       
      },
      listView: {
        marginTop: -15,
        marginHorizontal: 10,
      },
      card:{
        marginTop:10,
        marginLeft:5,
       backgroundColor: '#eee',
      borderWidth:1,
   borderRadius: 5,
     width: 400,
     height: 150,
   
    paddingTop:10,
    paddingLeft:10,
     },
    cardPosition:{
        flex:1,
        flexDirection:'column',
        justifyContent:'flex-start',
        marginTop:20,
    }

    
})







