import React from 'react';
import {View,Text,StyleSheet,TextInput,TouchableOpacity} from 'react-native';
import {Header} from "../components"


export default class Setting extends React.Component{

    static navigationOptions = ({navigation}) => ({
        headerLeft: <Header navigation={navigation}/>
    });
  

  render(){
      return(
          <View style={styles.pagecolor}>
           <Text style={{paddingTop:140, fontSize:30}}> Change Password  </Text>
            <View style={styles.card}>
                 <Text style={styles.HeadingText}> Old Password </Text>
                 <View style={styles.signupBox}>
                <TextInput
                style={styles.signupPlaceholderText} 
                placeholder='Enter Old Password'   
                />
                </View>

                <Text style={styles.HeadingText}> New Password </Text>
                 <View style={styles.signupBox}>
                <TextInput
                style={styles.signupPlaceholderText} 
                placeholder='Enter New Password'   
                />
                </View>
                  
        <View style={styles.loginView}>
        <TouchableOpacity style={styles.login}
        onPress={()=>{this.props.navigation.navigate('Setting')}}> 
        
              <Text style={styles.loginText}> Update </Text>
             </TouchableOpacity>
        </View>
                </View>
               
                
              </View>
      )
  }


}

const styles= StyleSheet.create({
    pagecolor: {
        backgroundColor: '#fff',
        flex:1,
        flexDirection:'column',
       
      },
      card:{
         marginTop:10,
        backgroundColor: '#eee',
       borderWidth:1,
    borderRadius: 5,
   alignSelf:'center',
      width: 400,
      height: 300,
     justifyContent:'space-evenly',
     paddingTop:10,
     paddingLeft:10,
      },
      HeadingText:{
        fontSize:20,
        fontWeight:'200',  
            },
    NormalText:{
    fontSize:20,
    fontWeight:'100',
    paddingLeft:10,  
        },
        signupBox:{
            height: 50,
            width: 365,
            borderColor: 'gray', 
            borderWidth: 1,
            alignSelf:'center',
            borderRadius:10,
            backgroundColor:'white'
           
        },
        signupPlaceholderText:{
            fontSize:20,
            fontWeight:'100',
            flexDirection:'row',
            justifyContent:'center',
            paddingLeft:20,
            paddingTop:5,
        },
        login:{
            height: 60,
            width: 280,
            alignSelf:'center',
            borderRadius:10,
            backgroundColor:'#EF5552',
        },
        loginView:{
            paddingTop:10
        },
        loginText:{
            alignSelf:'center',
            color:'white',
            fontSize:25,
            fontWeight:'bold',
            paddingTop:7
        },
})