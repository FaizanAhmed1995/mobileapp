import React from "react";
import {StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import {createAccount} from "../utils/apis";
export default class Signup extends React.Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {

            name: 'qwq',
            email: 'qwsdasqs12w@gmail.com',
            password: '12121212121',
            repeatpassword: '12121212121',
            id: '',
            referralLink: '',
            errors: {}
        };

        this.onChange = this.onChange.bind(this);

    }

    componentDidMount() {
        // check Auth status
    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    Submit = async (e) => {
        e.preventDefault();
        let UserSignup;
        if (this.state.referralLink == "") {
            UserSignup = {
                firstName: this.state.name,
                email: this.state.email,
                password: this.state.password,
                //referralLink: this.state.referralLink
                // repeatpassword:this.state.repeatpassword,

            };
            console.log(UserSignup);

        }
        else {
            UserSignup = {
                firstName: this.state.name,
                email: this.state.email,
                password: this.state.password,
                referralLink: this.state.referralLink
                // repeatpassword:this.state.repeatpassword,

            };
        }
        const resp = await createAccount(UserSignup);
        console.log('resp:   :', resp.data.message)
        if(resp.data.code === 200){
            this.props.navigation.navigate('Dashboard');
        }

    }

    render() {
        const {Submit} = this;
        const {errors} = this.state;

        return (
            <View style={styles.pagecolor}>
                <View >
                    <Text style={styles.CreateAccountText}> Create Account </Text>
                </View>
                <Text style={styles.signupText}> Name </Text>
                <View style={styles.signupBox}>
                    <TextInput
                        style={styles.signupPlaceholderText}
                        placeholder='Enter Name'
                        value={this.state.name}
                        onChange={this.onChange}
                    />
                </View>
                <Text style={styles.signupText}> Email </Text>
                <View style={styles.signupBox}>
                    <TextInput
                        style={styles.signupPlaceholderText}
                        placeholder='Enter Email'
                        value={this.state.email}
                        onChange={this.onChange}
                    />
                </View>
                <Text style={styles.signupText}> Password </Text>
                <View style={styles.signupBox}>
                    <TextInput
                        style={styles.signupPlaceholderText}
                        placeholder='Enter Password'
                        value={this.state.password}
                        onChange={this.onChange}
                    />
                </View>
                <Text style={styles.signupText}> Referral Link </Text>
                <View style={styles.signupBox}>
                    <TextInput
                        style={styles.signupPlaceholderText}
                        placeholder='Enter Referral Link'
                        value={this.state.referralLink}
                        onChange={this.onChange}
                    />
                </View>

                <View style={styles.loginView}>
                    <TouchableOpacity style={styles.login}
                                      onPress={Submit}>

                        <Text style={styles.loginText}> Create Account </Text>
                    </TouchableOpacity>
                </View>
                {errors.message && (
                    <View style={{color: 'red'}}>{errors.message} !</View>
                )}
                <View style={{paddingTop: 8}}>
                    <TouchableOpacity onPress={() => {
                        this.props.navigation.navigate('Signin')
                    }}>
                        <Text style={{alignSelf: 'center', color: 'black', fontSize: 15}}> Already have an
                            Account? </Text>
                    </TouchableOpacity>
                </View>
            </View>

        )
    }

}

const styles = StyleSheet.create({
    pagecolor: {
        backgroundColor: '#fff',
        flex: 1
    },
    CreateAccountText: {
        paddingTop: 60,
        flexDirection: 'row',
        justifyContent: 'center',
        fontSize: 50,
        fontWeight: 'bold',
        color: '#EF5552',
        paddingLeft: 15,
    },
    signupBox: {
        height: 50,
        width: 280,
        borderColor: 'gray',
        borderWidth: 1,
        alignSelf: 'center',
        borderRadius: 10,
        backgroundColor: 'white'

    },
    signupText: {
        fontSize: 25,
        fontWeight: '100',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingLeft: 65,
        paddingTop: 5,
        paddingBottom: 5,
        color: '#EF5552',
    },
    signupPlaceholderText: {
        fontSize: 20,
        fontWeight: '100',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingLeft: 20,
        paddingTop: 5,
    },
    login: {
        height: 60,
        width: 280,
        alignSelf: 'center',
        borderRadius: 10,
        backgroundColor: '#EF5552',
    },
    loginText: {
        alignSelf: 'center',
        color: 'white',
        fontSize: 25,
        fontWeight: 'bold',
        paddingTop: 7
    },
    loginView: {
        paddingTop: 10
    }
})