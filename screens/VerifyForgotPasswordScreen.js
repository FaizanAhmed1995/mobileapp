import React from 'react';
import {View,Text,StyleSheet,TextInput,TouchableOpacity} from 'react-native';


export default class VerifyForgotPassword extends React.Component{
    static navigationOptions = {
        header: null
      };
render()
{
return(
   <View style={styles.pagecolor}> 
    <View >  
        <Text style={styles.CreateAccountText}> Update Password   </Text>
    </View>
  
      
        <Text style={styles.signupText}> Email </Text>
        <View style={styles.signupBox}>
              <TextInput
              style={styles.signupPlaceholderText} 
              placeholder='Enter Email'   
              />
        </View>
        <Text style={styles.signupText}> Passcode </Text>
        <View style={styles.signupBox} >
              <TextInput 
              style={styles.signupPlaceholderText}
              placeholder='Enter Passcode'   
              />
        </View>
        <Text style={styles.signupText}> New Password </Text>
        <View style={styles.signupBox} >
              <TextInput 
              style={styles.signupPlaceholderText}
              placeholder='Enter New Password'   
              />
        </View>
       <View style={{flexDirection:'row',justifyContent:'space-evenly'}}>
       
       </View>
        <View style={styles.loginView}>
        <TouchableOpacity style={styles.login}
        onPress={()=>{this.props.navigation.navigate('VerifyForgotPassword')}}> 
        
              <Text style={styles.loginText}> Proceed </Text>
             </TouchableOpacity>
        </View>
       
   </View>

   )
}

}


const styles=StyleSheet.create({
    pagecolor: {
        backgroundColor: '#fff',
        flex:1
      },
      CreateAccountText:{
          paddingTop:130,
          paddingBottom:20,
          flexDirection:'row',
          justifyContent:'center',
          alignSelf:'center',
          fontSize:50,
          fontWeight:'bold',
          color:'#EF5552',
        },
        signupBox:{
            height: 50,
            width: 280,
            borderColor: 'gray', 
            borderWidth: 1,
            alignSelf:'center',
            borderRadius:10,
            backgroundColor:'white'
           
        },
        signupText:{
            fontSize:25,
            fontWeight:'100',
            flexDirection:'row',
            justifyContent:'center',
            paddingLeft:65,
            paddingTop:5,
            paddingBottom:5,
            color:'#EF5552',
        },
        signupPlaceholderText:{
            fontSize:20,
            fontWeight:'100',
            flexDirection:'row',
            justifyContent:'center',
            paddingLeft:20,
            paddingTop:5,
        },
        login:{
            height: 60,
            width: 280,
            alignSelf:'center',
            borderRadius:10,
            backgroundColor:'#EF5552',
            margin:10,
        },
        loginText:{
            alignSelf:'center',
            color:'white',
            fontSize:25,
            fontWeight:'bold',
            paddingTop:7
        },
        loginView:{
            paddingTop:10
        }
})