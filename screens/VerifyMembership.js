import React from 'react';
import {ScrollView,ListView,TouchableOpacity,StyleSheet,View,Text} from 'react-native';
import {Header} from "../components"



export default class VerifyMembership extends React.Component{
    static navigationOptions = ({navigation}) => ({
        headerLeft: <Header navigation={navigation}/>
    });

render(){

    return(
       <View style={styles.pagecolor}>
          <View style={styles.cardPosition}>
          <Text style={{fontSize:20, fontWeight:'400' }}> Verify Membership  </Text>
         <View style={styles.card}>
               <Text style={{fontSize:17 , fontWeight:'300'}}> Do you want to proceed with the selected Membership ? </Text>
                   
                    <View style={{flexDirection:'row', paddingTop:5,justifyContent:'space-evenly'}}>
                    <View style={styles.loginView}>
        <TouchableOpacity style={styles.login}
        onPress={()=>{this.props.navigation.navigate('VerifyMembership')}}> 
        
              <Text style={styles.loginText}> Yes </Text>
             </TouchableOpacity>
        </View>

           <View style={styles.loginView}>
        <TouchableOpacity style={styles.login}
        onPress={()=>{this.props.navigation.navigate('Membership')}}> 
        
              <Text style={styles.loginText}> No </Text>
             </TouchableOpacity>
        </View>
                        </View>
                   
                    </View>
              </View>
          </View>
    )
}


}

const styles=StyleSheet.create({
    pagecolor: {
        backgroundColor: '#fff',
        flex:1,
        flexDirection:'column',
       
      },
      
      card:{
        marginTop:10,
        marginLeft:5,
       backgroundColor: '#eee',
      borderWidth:1,
   borderRadius: 5,
     width: 400,
     height: 150,
   
    paddingTop:10,
    paddingLeft:10,
     },
    cardPosition:{
        flex:1,
        flexDirection:'column',
        justifyContent:'flex-start',
        marginTop:20,
    },
    login:{
        height: 40,
        width: 100,
        alignSelf:'center',
        borderRadius:14,
        backgroundColor:'#EF5552',
    },
    loginText:{
        alignSelf:'center',
        color:'white',
        fontSize:20,
        fontWeight:'bold',
        paddingTop:7
    },
    loginView:{
        paddingTop:10
    }

    
})







