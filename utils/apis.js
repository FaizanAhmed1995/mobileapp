import axios from 'axios';

export async function createAccount(payload) {
    return await axios({
        method: 'post',
        url: 'http://localhost:4000/api/user/signup',
        data: payload
    })

}
